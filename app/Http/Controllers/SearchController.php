<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
// use Illuminate\Support\Facades\Storage;
class SearchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function searchGlobal(Request $req)
    {

        // $get = DB::table('warehouse as a')
        //     ->select('a.*')
        //   // ->join('product_brand as b','a.brand_id','=','b.id')
        //   // ->select('a.name','a.item_no','a.code','b.name as brand','a.id','a.id as product_id','b.name as brand','c.code as unit','d.name as type','e.name as category','f.name as subcategory','g.name as group','h.qty as summarystock','sp.width','sp.length','sp.height','isibesar.unitname as unitbesar',DB::raw('ifnull(cast(h.qty/isibesar.qty as decimal(10,2)),0) as qtybesar'))
        //   ->where('a.country',$req->get('country'))
        //   ->orWhere('a.state',$req->get('state'))
        //   ->orWhere('a.city',$req->get('city'))
        //   // ->take(10)
        //   ->get();
        // $country = $req->input('country');
        // $state = $req->input('state');
        // // $city = $req->input('city');
        // if ($country != "") {
        // $get = DB::table('warehouse as a')
        //     ->where('country', 'like', '%'.$country.'%')
        //     ->get();    
        // }elseif ($country == "") {
        // $get = DB::table('warehouse as a')
        //     ->where('state', 'like', '%'.$state.'%')
        //     ->get();    
        // }
            $get = DB::table('warehouse as a')
        // ->whereRaw("(a.country like '%".$country."%' or a.state like '%".$state."%' or a.city like '%".$city."%')")
            ->where('country', 'like', '%'.$country.'%')
            ->orWhere('state', 'like', '%'.$state.'%')
            ->orWhere('city', 'like', '%'.$city.'%')
            ->get();
        // $get = DB::table('warehouse')->get();
        return Response()->json([
            'data' => $get
        ]);
    }
    
        public function getAllLocation(Request $req)
    {
            $get = DB::table('master_location_countries as a')
            ->select('a.code as code_country','a.name as name_country','b.name as name_state','c.name as name_city')
            ->leftjoin('master_location_states as b','b.country_id','=','a.id')
            ->leftjoin('master_location_cities as c','c.state_id','=','b.id')
            ->take(100)->get();
        return Response()->json([
            'data' => $get
        ]);
    }
}
