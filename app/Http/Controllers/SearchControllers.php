<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
// use Illuminate\Support\Facades\Storage;
class SearchController extends Controller
{

  public function searchWh()
    {
        $get = DB::table('warehouse')->get();
        return Response()->json([
            'data' => $get
        ]);
    }
    public function getData(Request $reg)
    {
        if($reg->get('c')!=''){
          if($reg->get('c')=='Available'){
            $w ="h.summarystock >0";
            $w2 = "h.stockavl >0";
          }else{
            $w ="(h.summarystock <=0 or h.summarystock is null)";
            $w2 = "(h.stockavl <=0 or h.stockavl is null or h.warehouse_id is null)";
          }
        }else{
          $w ="(h.summarystock <>0 or h.summarystock=0)";
          $w2 = "(h.stockavl <>0 or h.stockavl=0)";
        }
        if($reg->get('wh')!=''){
          $products = DB::table('product as a')
          ->join('product_brand as b','a.brand_id','=','b.id')
          ->join('unit as c','a.unit_id','=','c.id')
          ->join('product_type as d','a.type_id','=','d.id')
          ->join('product_category as e','a.category_id','=','e.id')
          ->join('product_sub_category as f','a.sub_cat_id','=','f.id')
          ->join('product_group as g','a.group_id','=','g.id')
          ->join('product_spec as sp','a.id','=','sp.product_id')
          ->leftjoin('stockavl as h','a.id','=','h.product_id')
          ->join('vw_inv_serial_stock as st','a.id','=','st.product_id')
          ->select('a.name','a.item_no','a.code','b.name as brand','a.id','a.id as product_id','b.name as brand','c.code as unit','d.name as type','e.name as category','f.name as subcategory','g.name as group','h.stockavl as summarystock','sp.width','sp.length','sp.height','st.stockavl as stock')
          ->where('h.warehouse_id',$reg->get('wh'))
          ->whereRaw($w2)
          //->take(50)
          ->get();
          //print_r($products);
          //exit;
        }else{
          $products =DB::table('product as a')
              ->join('product_brand as b','a.brand_id','=','b.id')
              ->join('unit as c','a.unit_id','=','c.id')
              ->join('product_type as d','a.type_id','=','d.id')
              ->join('product_category as e','a.category_id','=','e.id')
              ->join('product_sub_category as f','a.sub_cat_id','=','f.id')
              ->join('product_group as g','a.group_id','=','g.id')
              ->join('product_spec as sp','a.id','=','sp.product_id')
              // ->join('product_image as h','a.id','=','h.product_id')
              ->leftjoin('vw_stock_new as h','a.id','=','h.product_id')
              ->leftjoin('vw_cogs_history as i','a.id','=','i.product_id')
              ->join('vw_inv_serial_stock as st','a.id','=','st.product_id')
              ->select('a.id','a.name','a.item_no','a.code','b.name as brand','h.*','i.price_balance','c.code as unit','d.name as type','e.name as category','f.name as subcategory','g.name as group','sp.width','sp.length','sp.height','st.stockavl as stock')
              ->whereNull('a.deleted_at')
              ->whereRaw($w)
              ->orderBy('a.code','asc')
              ->get();
        }

        // $dataimg = DB::table('product_image')->where('product_id',$id)->get();
        //$products = Product::all();
        //return response()->json($products, 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        return Datatables::of($products)
            ->addColumn('barcode',function($products){
              return '<img src="data:image/png;base64,'.DNS1D::getBarcodePNG($products->code, "C128").'" width="160px;" height="50px;" alt="barcode" />';
            })
            ->addColumn('action', function ($products) {
              $delete = '';
              if(Auth::user()->id!=8){
                $delete='<li>
                    <a href="#" class="delete" data-id="'.$products->id.'">
                        <i class="icon-docs"></i> Delete </a>
                </li>';
                $action = '<div class="btn-group">
                                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                    <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li>
                                        <a class="upload" data-id="'.$products->id.'" href="'.url('product/upload/'.$products->id).'" >
                                            <i class="fa fa-cloud"></i> Upload </a>
                                    </li>
                                    <li>
                                        <a class="upload2" data-id="'.$products->id.'" href="'.url('produk/diUpload/'.$products->id).'">
                                        <i class="fa fa-cloud"></i> Upload 2 </a>
                                    </li>
                                    '.$delete.'
                                </ul>
                            </div>';
              }else{
                $action = '
                    <a class="upload" data-id="'.$products->id.'" href="'.url('product/upload/'.$products->id).'" >
                        <i class="fa fa-cloud"></i> Upload </a>
                    <a class="upload2" data-id="'.$products->id.'" href="'.url('produk/diUpload/'.$products->id).'">
                        <i class="fa fa-cloud"></i> Upload 2 </a> 
                    </a>
                ';
              }
                return $action;
            })
            ->make(true);
    }
    // public function getDataNew(Request $reg)
    public function getData2(Request $reg)
    {
         if($reg->get('stockavl')!=''){
          if($reg->get('stockavl')=='0'){
            $w ="(h.stockavl <>0 or h.stockavl=0 or h.stockavl is null)";
            $w2 = "(h.stockavl <>0 or h.stockavl=0 or h.stockavl is null)";
          }elseif($reg->get('stockavl')=='1'){
            $w ="h.stockavl >0";
            $w2 = "h.stockavl >0";
          }else{
            $w ="(h.stockavl <=0 or h.stockavl is null)";
            $w2 = "(h.stockavl <=0 or h.stockavl is null)";
          }
        }else{
          $w ="(h.stockavl <>0 or h.stockavl=0 or h.stockavl is null)";
          $w2 = "(h.stockavl <>0 or h.stockavl=0 or h.stockavl is null)";
        }
        if($reg->get('tcari')!=''){
            $tglobal="(a.code like '%".$reg->get('tcari')."%' or b.name like '".$reg->get('tcari')."' or e.name like '".$reg->get('tcari')."' or a.name like '".$reg->get('tcari')."')";
        }else{
          $tglobal="1=1"; 
        }
        if($reg->get('wh')!=''){
          $products = DB::table('product as a')
          ->join('product_brand as b','a.brand_id','=','b.id')
          ->join('unit as c','a.unit_id','=','c.id')
          ->join('product_type as d','a.type_id','=','d.id')
          ->join('product_category as e','a.category_id','=','e.id')
          ->join('product_sub_category as f','a.sub_cat_id','=','f.id')
          ->join('product_group as g','a.group_id','=','g.id')
          ->join('product_spec as sp','a.id','=','sp.product_id')
          ->leftjoin('stockavl as h','a.id','=','h.product_id')
          ->leftjoin('vwisibesarqty as isibesar','isibesar.product_id','=','a.id')
          ->select('a.name','a.item_no','a.code','b.name as brand','a.id','a.id as product_id','b.name as brand','c.code as unit','d.name as type','e.name as category','f.name as subcategory','g.name as group','h.stockavl as summarystock','sp.width','sp.length','sp.height','isibesar.unitname as unitbesar',DB::raw('ifnull(cast(h.stockavl/isibesar.qty as decimal(10,2)),0) as qtybesar'))
          ->where('h.warehouse_id',$reg->get('wh'))
          ->whereRaw($w2)
          //->take(50)
          ->get();
          //print_r($products);
          //exit;
        }else{
            $products =DB::table('product as a')
              ->join('product_brand as b','a.brand_id','=','b.id')
              ->join('unit as c','a.unit_id','=','c.id')
              ->join('product_type as d','a.type_id','=','d.id')
              ->join('product_category as e','a.category_id','=','e.id')
              ->join('product_sub_category as f','a.sub_cat_id','=','f.id')
              ->join('product_group as g','a.group_id','=','g.id')
              ->join('product_spec as sp','a.id','=','sp.product_id')
              // ->join('product_image as h','a.id','=','h.product_id')
              ->leftjoin('stockavlall as h','a.id','=','h.product_id')
              ->leftjoin('vwisibesarqty as isibesar','isibesar.product_id','=','a.id')
              //->leftjoin('vw_cogs_history as i','a.id','=','i.product_id')
              ->select('a.id','a.name','a.item_no','a.code','b.name as brand',DB::raw('ifnull(h.stockavl,0) as summarystock'),'h.product_id','c.code as unit','d.name as type','e.name as category','f.name as subcategory','g.name as group','sp.width','sp.length','sp.height','isibesar.unitname as unitbesar',DB::raw('ifnull(cast(h.stockavl/isibesar.qty as decimal(10,2)),0) as qtybesar'))
              ->whereNull('a.deleted_at')
              ->whereRaw($w)
              ->whereRaw($tglobal)
              ->orderBy('a.id','asc')
              ->take(100)
              ->get();
        }
        // return response()->json(['data'=>$products]);

        // $dataimg = DB::table('product_image')->where('product_id',$id)->get();
        //$products = Product::all();
        return response()->json([
          'msg'=>$products
        ]);
        
    }
    public function getbarcode($code){
        return Response()->json([
          'img'=>DNS1D::getBarcodePNG($code, "C128")
        ]);
    }
    public function getDatabyserial(Request $reg)
    {
        ini_set('max_execution_time', 120);
        if($reg->get('stockavl')!=''){
          if($reg->get('stockavl')=='0'){
            $w ="(h.qty <>0 or h.qty=0 or h.qty is null)";
            $w2 = "(h.qty <>0 or h.qty=0 or h.qty is null)";
          }elseif($reg->get('stockavl')=='1'){
            $w ="h.qty >0";
            $w2 = "h.qty >0";
          }else{
            $w ="(h.qty <=0 or h.qty is null)";
            $w2 = "(h.qty <=0 or h.qty is null)";
          }
        }else{
          $w ="(h.qty <>0 or h.qty=0 or h.qty is null)";
          $w2 = "(h.qty <>0 or h.qty=0 or h.qty is null)";
        }
        if($reg->get('tcari')!=''){
            $tglobal="(a.code like '%".$reg->get('tcari')."%' or b.name like '".$reg->get('tcari')."' or e.name like '".$reg->get('tcari')."' or a.name like '".$reg->get('tcari')."')";
        }else{
          $tglobal="1=1";
        }
        if($reg->get('wh')!=''){
          $products = DB::table('product as a')
          ->join('product_brand as b','a.brand_id','=','b.id')
          ->join('unit as c','a.unit_id','=','c.id')
          ->join('product_type as d','a.type_id','=','d.id')
          ->join('product_category as e','a.category_id','=','e.id')
          ->join('product_sub_category as f','a.sub_cat_id','=','f.id')
          ->join('product_group as g','a.group_id','=','g.id')
          ->join('product_spec as sp','a.id','=','sp.product_id')
          ->leftjoin('vw_serial_box_detail as h','a.id','=','h.product_id')
          ->leftjoin('vwisibesarqty as isibesar','isibesar.product_id','=','a.id')
          ->select('a.name','a.item_no','a.code','b.name as brand','a.id','a.id as product_id','b.name as brand','c.code as unit','d.name as type','e.name as category','f.name as subcategory','g.name as group','h.qty as summarystock','sp.width','sp.length','sp.height','isibesar.unitname as unitbesar',DB::raw('ifnull(cast(h.qty/isibesar.qty as decimal(10,2)),0) as qtybesar'))
          ->where('h.warehouse_id',$reg->get('wh'))
          ->whereRaw($w2)
          ->take(100)
          // ->paginate(50)
          ->get();
          //print_r($products);
          //exit;
        }else{
          $products =DB::table('product as a')
              ->join('product_brand as b','a.brand_id','=','b.id')
              ->join('unit as c','a.unit_id','=','c.id')
              ->join('product_type as d','a.type_id','=','d.id')
              ->join('product_category as e','a.category_id','=','e.id')
              ->join('product_sub_category as f','a.sub_cat_id','=','f.id')
              ->join('product_group as g','a.group_id','=','g.id')
              ->join('product_spec as sp','a.id','=','sp.product_id')
              // ->join('product_image as h','a.id','=','h.product_id')
              ->leftjoin('vw_serial_box_detail_no_wh as h','a.id','=','h.product_id')
              ->leftjoin('vwisibesarqty as isibesar','isibesar.product_id','=','a.id')
              //->leftjoin('vw_cogs_history as i','a.id','=','i.product_id')
              ->select('a.id','a.name','a.item_no','a.code','b.name as brand',DB::raw('ifnull(h.qty,0) as summarystock'),'h.product_id','c.code as unit','d.name as type','e.name as category','f.name as subcategory','g.name as group','sp.width','sp.length','sp.height','isibesar.unitname as unitbesar',DB::raw('ifnull(cast(h.qty/isibesar.qty as decimal(10,2)),0) as qtybesar'))
              ->whereNull('a.deleted_at')
              ->whereRaw($w)
              ->whereRaw($tglobal)
              ->orderBy('a.id','asc')
              // ->take(10)
              ->paginate(100);
              // ->get();
        }

        return Response()->json([
            'msg'=>$products
        ]);
        // $dataimg = DB::table('product_image')->where('product_id',$id)->get();
        //$products = Product::all();
        //return response()->json($products, 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        // return Datatables::of($products)
        //     ->addColumn('barcode',function($products){
        //       return '<img src="data:image/png;base64,'.DNS1D::getBarcodePNG($products->code, "C128").'" width="160px;" height="50px;" alt="barcode" />';
        //     })
        //     ->addColumn('action', function ($products) {
        //       $delete = '';
        //       if(Auth::user()->id!=8){
        //         $delete='<li>
        //             <a href="#" class="delete" data-id="'.$products->id.'">
        //                 <i class="icon-docs"></i> Delete </a>
        //         </li>';
        //         $action = '<div class="btn-group">
        //                         <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
        //                             <i class="fa fa-angle-down"></i>
        //                         </button>
        //                         <ul class="dropdown-menu pull-right" role="menu">
        //                             <li>
        //                                 <a class="upload" data-id="'.$products->id.'" href="'.url('product/upload/'.$products->id).'" >
        //                                     <i class="fa fa-cloud"></i> Upload </a>
        //                             </li>
        //                             <li>
        //                                 <a class="upload2" data-id="'.$products->id.'" href="'.url('produk/diUpload/'.$products->id). '">
        //                                   <i class="fa fa-cloud"></i> Upload 2 </a>
        //                             </li>
        //                             '.$delete.'
        //                         </ul>
        //                     </div>';
        //       }else{
        //         $action = '
        //             <a class="upload" data-id="'.$products->id.'" href="'.url('product/upload/'.$products->id).'" >
        //                 <i class="fa fa-cloud"></i> Upload </a>
        //             <a class="upload2" data-id="'.$products->id.'" href="'.url('produk/diUpload/'.$products->id).'" >
        //                 <i class="fa fa-cloud"></i> Upload 2 </a>
        //         ';
        //       }
        //         return $action;
        //     })
        //     ->make(true);
    }
    public function getSelect2Unit(Request $request){
        $search = $request->q;
        $data =DB::table('product as a')
            ->join('unit as c','a.unit_id','=','c.id')
            ->select('a.id','c.name as text')
            ->whereNull('a.deleted_at')
            ->orderBy('c.name','asc')
            ->groupBy('c.name')
            ->where('c.name','like',"%$search%")
            ->get();
        return response()->json($data);
    }
    public function getSelect2Type(Request $request){
        $search = $request->q;
        $data =DB::table('product as a')
            ->join('product_type as d','a.type_id','=','d.id')
            ->where('')
            ->select('a.id','d.name as text')
            ->whereNull('a.deleted_at')
            ->orderBy('d.name','asc')
            ->groupBy('d.name')
            ->where('d.name','like',"%$search%")
            ->get();
        return response()->json($data);
    }
    public function getSelect2Category(Request $request){
        $search = $request->q;
        $data =DB::table('product as a')
            ->join('product_category as e','a.category_id','=','e.id')
            ->select('a.id','e.name as text')
            ->whereNull('a.deleted_at')
            ->orderBy('e.name','asc')
            ->groupBy('e.name')
            ->where('e.name','like',"%$search%")
            ->get();
        return response()->json($data);
    }
    public function getSelect2SubCat(Request $request){
        $search = $request->q;
        $data =DB::table('product as a')
            ->join('product_category as e','a.category_id','=','e.id')
            ->join('product_sub_category as f','a.sub_cat_id','=','f.id')
            ->select('a.id','f.name as text')
            ->whereNull('a.deleted_at')
            ->orderBy('f.name','asc')
            ->groupBy('f.name')
            ->where('f.name','like',"%$search%")
            ->get();
        return response()->json($data);
    }
    public function getSelect2Brand(Request $request){
        $search = $request->q;
        $data =DB::table('product as a')
            ->join('product_brand as b','a.brand_id','=','b.id')
            ->select('a.id','b.name as text')
            ->whereNull('a.deleted_at')
            ->orderBy('b.name','asc')
            ->groupBy('b.name')
            ->where('b.name','like',"%$search%")
            ->get();
        return response()->json($data);
    }
    public function getSelect2Group(Request $request){
        $search = $request->q;
        $data =DB::table('product as a')
            ->join('product_group as g','a.group_id','=','g.id')
            ->select('a.id','g.name as text')
            ->whereNull('a.deleted_at')
            ->orderBy('g.name','asc')
            ->groupBy('g.name')
            ->where('g.name','like',"%$search%")
            ->get();
        return response()->json($data);
    }
    public function getDataRecycle()
    {
        $products =DB::table('product as a')
            ->join('product_brand as b','a.brand_id','=','b.id')
            ->join('unit as c','a.unit_id','=','c.id')
            ->join('product_type as d','a.type_id','=','d.id')
            ->join('product_category as e','a.category_id','=','e.id')
            ->join('product_sub_category as f','a.sub_cat_id','=','f.id')
            ->join('product_group as g','a.group_id','=','g.id')
            ->leftjoin('vw_summary_stock as h','a.id','=','h.product_id')
            ->leftjoin('vw_cogs_history as i','a.id','=','i.product_id')
            ->select('a.*','b.name as brand','h.*','i.*','c.name as unit','d.name as type','e.name as category','f.name as subcategory','g.name as group')
            ->whereNotNull('a.deleted_at')
            ->get();
        //$products = Product::all();
        return Datatables::of($products)
            ->addColumn('price', function ($products) {
                return number_format($products->price);
            })
            ->addColumn('action', function ($products) {
                return
                    '<div class="btn-group">
                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-left" role="menu">
                                        <li>
                                            <a href="product/detail_product/'.$products->id.'" data-toggle="modal">
                                                <i class="icon-tag"></i> Detail </a>
                                        </li>
                                        <li>
                                            <a href="#" class="restore" data-id="'.$products->id.'">
                                                <i class="icon-docs"></i> Restore </a>
                                        </li>
                                        <li>
                                            <a href="#" class="deletePermanent" data-id="'.$products->id.'">
                                                <i class="icon-docs"></i> Delete </a>
                                        </li>
                                    </ul>
                                </div>';
            })
            ->make(true);

    }
    public function insertproduct(){
        $code = MegaTrend::getLastCode("PR",'product','code');
        $brands = ProductBrand::all();
        $categorys=ProductCategory::all();
        $types = ProductType::all();
        $units = Unit::all();
        $groups = ProductGroup::all();
        $branch = DB::table('branch')->get();
        return view('modules.product.insert_product',compact('branch','brands','code','categorys','units','types','groups'));
    }
    public function insertproductv2(){
        $itemno = MegaTrend::getProductItem("SKU",'product','item_no');
        // $code = MegaTrend::getLastCode("PR",'product','code');
        $brands = ProductBrand::all();
        $categorys=ProductCategory::all();
        $types = ProductType::all();
        $units = Unit::all();
        $groups = ProductGroup::all();
        $branch = DB::table('branch')->get();
        return view('modules.product.insertv2',compact('itemno','branch','brands','code','categorys','units','types','groups'));
    }
    public function insertproductnew(){
        $itemno = MegaTrend::getProductItem("SKU",'product','item_no');
        // $code = MegaTrend::getLastCode("PR",'product','code');
        $brands = ProductBrand::all();
        $categorys=ProductCategory::all();
        $types = ProductType::all();
        $units = Unit::all();
        $groups = ProductGroup::all();
        $branch = DB::table('branch')->get();
        return view('modules.product.productnewInsert',compact('itemno','branch','brands','code','categorys','units','types','groups'));
    }
    public function getSubCategory($id){
        $subscat = ProductSubCategory::find(['category_id'=>$id]);
        //echo json_encode($subscat);
        return Response()->json(
            [ 'msg' => $subscat]
        );
    }
    public function detailproduct($id){
        $data = DB::table('product as a')
            ->leftjoin('product_type as b','b.id','=','a.type_id')
            ->leftjoin('product_group as c','c.id','=','a.group_id')
            ->leftjoin('product_brand as d','d.id','=','a.brand_id')
            ->leftjoin('product_category as e','e.id','=','a.category_id')
            ->leftjoin('product_sub_category as f','f.id','=','a.sub_cat_id')
            ->leftjoin('unit as g','g.id','=','a.unit_id')
            ->where('a.id','=',$id)
            ->select('a.*','b.name as type','c.name as group','d.name as brand','e.name as category','f.name as sub_category','g.name as unit')
            ->get()->first();
        return view('modules.dashboard.product_dashboard',compact('data'));
    }
    public function detailproductnew($id){
        $data = DB::table('product as a')
            ->leftjoin('product_type as b','b.id','=','a.type_id')
            ->leftjoin('product_group as c','c.id','=','a.group_id')
            ->leftjoin('product_brand as d','d.id','=','a.brand_id')
            ->leftjoin('product_category as e','e.id','=','a.category_id')
            ->leftjoin('product_sub_category as f','f.id','=','a.sub_cat_id')
            ->leftjoin('unit as g','g.id','=','a.unit_id')
            ->leftjoin('product_detail as h','h.product_id','=','a.id')
            ->leftjoin('vendor as i','i.id','=','h.vendor_id')
            ->leftjoin('tax as j','j.id','=','tax_id')
            ->leftjoin('product_spec as k','a.id','=','k.product_id')
            ->leftjoin('product_coa as l','a.id','=','l.product_id')
            ->leftjoin('coa_list as m','l.coa_list_id','m.id')
            ->where('a.id','=',$id)
            ->select('a.*','b.name as type','c.name as group','d.name as brand','e.name as category','f.name as sub_category','g.name as unit','i.name as ven','j.name as tax','k.width','k.weight','k.length','k.height','m.name as coa')
            ->get()->first();
        return view('modules.product.productnewdetail',compact('data'));
    }
    public function restoreData($id){
        $salesman = Product::withTrashed()
            ->where('id', $id);
        if($salesman->restore()) {
            return Response()->json(
                ['status' => true, 'msg' => 'Data has restored!','type'=>'success','title'=>'Success']
            );
        }else{
            return Response()->json(
                ['status' => false, 'msg' => 'Something went wrong!', 'type' => 'warning', 'title' => 'Ops']
            );
        }
    }
    public function editproduct($id){
        $product = Product::find($id);
        $brands = ProductBrand::all();
        $categorys=ProductCategory::all();
        $subcat = ProductSubCategory::all();
        $types = ProductType::all();
        $units = Unit::all();
        $groups = ProductGroup::all();
        $branch = DB::table('branch')->get();
        $spec = ProductSpec::all();
        $unitcon = DB::table('vwunitcon as a')
        ->select('product_id','b.name','c.name','qty','a.unit_id as id')
        ->join('product as b','a.product_id','=','b.id')
        ->join('unit as c','c.id','=','a.unit_id')
        ->where('a.product_id',$product->id)
        ->whereNotNull('c.name')
        ->orderBy('product_id','c.id')
        ->get();
        $productcoa = CoaList::all();
        $procoa = DB::table('product_coa as a')
        ->select('a.product_id as id','b.code','b.name','a.coa_list_id','a.module_index_id')
        ->leftjoin('coa_list as b','a.coa_list_id','=','b.id')
        ->leftjoin('module_index as c','a.module_index_id','=','c.id')
        ->where('product_id',$product->id)
        ->get();

        // dd($procoa);
        $spec = ProductSpec::all();
        $productdetail = ProductDetail::all();
        $prodetail = DB::table('product_detail as a')
        ->select('d.name as tax','c.code','c.name as ven','a.vendor_id','a.tax_id','a.id')
        ->leftjoin('product as b','b.id','=','a.product_id')
        ->leftjoin('vendor as c','c.id','=','a.vendor_id')
        ->leftjoin('tax as d','d.id','=','a.tax_id')
        ->where('product_id',$product->id)
        ->get();

    //     foreach($procoa as $project){
    // $tagged_project_item[] = array(
    //     'id'    => ($project[0]->id),
    //     'title' => $project[0]->title,
    //     'text'  => $project[0]->text,
    //     'extra' => $project[0]->extra
    // );
        // echo json_encode($prodetail);
        // exit();

        return view('modules.product.editv2_product',compact('prodetail','spec','procoa','unitcon','branch','brands','categorys','units','types','groups','product'));
    }
    public function editproductnew($id){
        $product = Product::find($id);
        $brands = ProductBrand::all();
        $categorys=ProductCategory::all();
        $subcat = ProductSubCategory::all();
        $types = ProductType::all();
        $units = Unit::all();
        $groups = ProductGroup::all();
        $branch = DB::table('branch')->get();
        $spec = ProductSpec::all();
        $unitcon = DB::table('vwunitcon as a')
        ->select('product_id','b.name','c.name','qty','a.unit_id as id')
        ->join('product as b','a.product_id','=','b.id')
        ->join('unit as c','c.id','=','a.unit_id')
        ->where('a.product_id',$product->id)
        ->whereNotNull('c.name')
        ->orderBy('product_id','c.id')
        ->get();
        $productcoa = CoaList::all();
        $procoa = DB::table('product_coa as a')
        ->select('a.product_id as id','b.code','b.name','a.coa_list_id','a.module_index_id')
        ->leftjoin('coa_list as b','a.coa_list_id','=','b.id')
        ->leftjoin('module_index as c','a.module_index_id','=','c.id')
        ->where('product_id',$product->id)
        ->get();

        // dd($procoa);
        $spec = ProductSpec::all();
        $productdetail = ProductDetail::all();
        $prodetail = DB::table('product_detail as a')
        ->select('d.name as tax','c.code','c.name as ven','a.vendor_id','a.tax_id','a.id')
        ->leftjoin('product as b','b.id','=','a.product_id')
        ->leftjoin('vendor as c','c.id','=','a.vendor_id')
        ->leftjoin('tax as d','d.id','=','a.tax_id')
        ->where('product_id',$product->id)
        ->get();

    //     foreach($procoa as $project){
    // $tagged_project_item[] = array(
    //     'id'    => ($project[0]->id),
    //     'title' => $project[0]->title,
    //     'text'  => $project[0]->text,
    //     'extra' => $project[0]->extra
    // );
        // echo json_encode($prodetail);
        // exit();
        $data = DB::table('product as a')
            ->leftjoin('product_type as b','b.id','=','a.type_id')
            ->leftjoin('product_group as c','c.id','=','a.group_id')
            ->leftjoin('product_brand as d','d.id','=','a.brand_id')
            ->leftjoin('product_category as e','e.id','=','a.category_id')
            ->leftjoin('product_sub_category as f','f.id','=','a.sub_cat_id')
            ->leftjoin('unit as g','g.id','=','a.unit_id')
            ->leftjoin('product_detail as h','h.product_id','=','a.id')
            ->leftjoin('vendor as i','i.id','=','h.vendor_id')
            ->leftjoin('tax as j','h.tax_id','=','j.id')
            ->leftjoin('product_spec as k','a.id','=','k.product_id')
            ->leftjoin('product_coa as l','a.id','=','l.product_id')
            ->leftjoin('coa_list as m','l.coa_list_id','m.id')
            ->where('a.id','=',$id)
            ->select('a.*','b.name as type','c.name as group','d.name as brand','e.name as category','f.name as sub_category','g.name as unit','i.name as ven','j.name as tax','k.width','k.weight','k.length','k.height','m.name as coa')
            ->get()->first();

        return view('modules.product.productnewEdit',compact('prodetail','spec','procoa','unitcon','branch','brands','categorys','units','types','groups','product','data'));
    }
    public function getProduct(Request $request){
        $find = $request->input('product');
        $product = DB::table('product')
            ->where('name', 'like', '%'.strtoupper($find).'%')
            ->orWhere('code', 'like', '%'.strtoupper($find).'%')
            ->get();
    //     $product=DB::select("
    //     select a.*,concat('http://103.200.4.92/image/',filename) as file from product a
    // left join
    // (
    // select filename,product_id from product_image group by product_id
    // ) b on a.id=b.product_id where (name like '%".strtoupper($find)."%' or code like '%".strtoupper($find)."%')
    //     ")
        return Response()->json($product);
    }
    public function addDatas(Request $request ){
        $code=$request->input('code');
        $name=$request->input('name');
        $unit=$request->input('unit');
        $type=$request->input('type');
        $brand=$request->input('brand');
        $category=$request->input('category');
        $subCategory=$request->input('subcategory');
        $group = $request->input('group');
        $maxPaymentPeriode = $request->input('maxpaymentperiode');
        //exit;
        $stockmin = $request->input('stokmin');
        $price = $request->input('itemno');
        $sql="call spins_product('".$code."','".$name."','".$unit."','".$type."','".$category."','".$subCategory."','".$brand."','".$group."','".$maxPaymentPeriode."','".$stockmin."','".$price."','".$request->input('branch') ."')";

        try {
            $save =DB::statement($sql);
        }catch (\Exception $e){
            $save=0;
        }
       // print_r($brand);
        if($save) {
            return Response()->json(
                ['status' => true, 'msg' => 'Success','type'=>'success']
            );
        }else{
            return Response()->json(
                ['status' => false, 'msg' => 'Failed','type'=>'warning']
            );
        }
    }
    public function updateDatas(Request $request,$id){
        if($request->isJson()){
             $sql = "call spupd_product('" . $request->json('code') . "','" . $request->json('name') . "','" . $request->json('unit') . "',
       '" . $request->json('type') . "','" . $request->json('category') . "','" . $request->json('subcategory') . "','" . $request->json('brand') . "',
       '" . $request->json('group') . "','" . $request->json('maxpaymentperiode') . "','" . $request->json('stokmin') . "','" . $id . "')";
            //exit;
        }else {
            $sql = "call spupd_product('" . $request->input('code') . "','" . $request->input('name') . "','" . $request->input('unit') . "',
       '" . $request->input('type') . "','" . $request->input('category') . "','" . $request->input('subcategory') . "','" . $request->input('brand') . "',
       '" . $request->input('group') . "','" . $request->input('maxpaymentperiode') . "','" . $request->input('stokmin') . "','" . $request->input('itemno') . "','" . $request->input('branch') . "','" . $id . "')";
        }

        try {
            $save =DB::statement($sql);
        }catch (\Exception $e){
            $save=0;
            //echo $e->getMessage();
        }
        // print_r($brand);
        //$save=1;
        if($save) {
            return Response()->json(
                ['status' => true, 'msg' => 'Product has updated','type'=>'success']
            );
        }else{
            return Response()->json(
                ['status' => false, 'msg' => 'Something went wrong!!','type'=>'warning']
            );
        }
    }
    public function deleteData($id){
        try {
            $save = Product::where('id',$id)->delete();
        }catch (\Exception $e){
            $save=0;
        }
        if($save) {
            return Response()->json(
                ['status' => true, 'msg' => 'Product has Deleted!','type'=>'success','title'=>'Success']
            );
        }else{
            return Response()->json(
                ['status' => false, 'msg' => 'Something went wrong!','type'=>'warning','title'=>'Ops']
            );
        }
    }
    public function getPriceProduct($id){
        $product = Product::find($id);
        return Response()->json(
            ['price' => (int)$product->price]
        );
    }
    public function getAllDataReturn(Request $request){
        $data = DB::select("select a.id,a.unit_id,a.name,a.item_no,a.code,b.price_balance as cogs_balance,cast(c.selling_price *1.1 as decimal(10,2)) selling_price,0 reg_disc_1,0 reg_disc2,d.code as unit_name,e.qty as qtykali  from product a 
left join vw_cogs_history b on a.id = b.product_id
left join (select * from vw_last_price_st where customer_id = ".$request->get('customerid').") c on c.product_id = a.id
left join unit d on d.id =a.unit_id
left join vwunitcon e on e.unit_id = a.unit_id and e.product_id = a.id
where (a.code like '%".$request->get('code')."%' or a.name like '%".$request->get('code')."%')
");
          return Response()->json([
            'data'=>$data
        ]);
    }
    public function getAllDataptReturn(Request $request){
        ini_set('max_execution_time', 120);
        $data = DB::select("select a.id,a.unit_id,a.name,a.item_no,a.code,b.price_balance as cogs_balance,cast(c.price *1.1 as decimal(10,2)) selling_price,0 reg_disc_1,0 reg_disc2,d.code as unit_name,e.qty as qtykali  from product a 
left join vw_cogs_history b on a.id = b.product_id
left join (select * from vw_last_price) c on c.product_id = a.id
left join unit d on d.id =a.unit_id
left join vwunitcon e on e.unit_id = a.unit_id and e.product_id = a.id
where (a.code like '%".$request->get('code')."%' or a.name like '%".$request->get('code')."%')
");
          return Response()->json([
            'data'=>$data
        ]);
    }
    public function getAllData(Request $request){
        ini_set('max_execution_time', 120);
        $noit = explode(',',$request->get('no_id'));
        if($request->get('all')!=null){
          $data = DB::table('product as a')
              ->leftjoin('selling_price_detail as b','a.id','=','b.product_id')
              ->leftjoin('selling_price as c','c.id','=','b.selling_price_id')
              ->leftjoin('unit as d','a.unit_id','=','d.id')
              ->leftjoin('vwunitcon as e',function($q){
                $q->on('a.unit_id','=','e.unit_id')->on('a.id','e.product_id');
              })
              ->leftjoin('stockavl as stk',function($q){
                $q->on('stk.product_id','=','a.id')
                ->on('stk.branch_id','=','c.branch_id');
              })
              ->leftjoin('vw_cogs_history as cogs',function($q){
                $q->on('cogs.product_id','=','a.id')
                ->on('cogs.branch_id','=','c.branch_id');
              })
              ->whereRaw("(a.code like '%".$request->get('code')."%' or a.name like '%".$request->get('code')."%')")
              //->orWhere('a.name','like', '%' .$request->get('code').'%')
              ->where('c.branch_id',$request->get('branch_id'))
              ->where('c.customer_group_id',$request->get('customer_group_id'))
              ->where('stk.warehouse_id',$request->get('warehouse_id'))
              //->where('stk.stockavl','>',0)
              ->whereNull('a.deleted_at')
              ->whereRaw("c.date = (select max(date) from selling_price where customer_group_id='".$request->get('customer_group_id')."')")
              ->select('a.*','b.price as selling_price','b.reg_disc_1','cogs.price_balance as cogs_balance','b.reg_disc_2','d.code as unit_name','e.qty as qtykali')
              ->distinct()
              ->take(5)
              ->whereNotin('a.id',$noit)
              ->orderBy('a.item_no','asc')
              ->get();
        }else{
          if($request->get('pt')!=null){
            $data = DB::table('product as a')
                //->leftjoin('selling_price_detail as b','a.id','=','b.product_id')
                //->leftjoin('selling_price as c','c.id','=','b.selling_price_id')
                ->leftjoin('unit as d','a.unit_id','=','d.id')
                ->leftjoin('vwunitcon as e',function($q){
                  $q->on('a.unit_id','=','e.unit_id')->on('a.id','e.product_id');
                })
                ->leftjoin('vw_cogs_history as cogs',function($q){
                  $q->on('cogs.product_id','=','a.id');
                  //->on('cogs.branch_id','=','c.branch_id');
                })
                ->whereRaw("(a.code like '%".$request->get('code')."%' or a.name like '%".$request->get('code')."%')")
                //->orWhere('a.name','like', '%' .$request->get('code').'%')
                ->where('cogs.branch_id',$request->get('branch_id'))
                //->where('c.customer_group_id',$request->get('customer_group_id'))
                ->whereNull('a.deleted_at')
                //->whereRaw('c.date = (select max(date) from selling_price)')
                //->where('stk.warehouse_id',$request->get('warehouse_id'))
                ->select('a.*','cogs.price_balance as selling_price','d.code as unit_name','e.qty as qtykali')
                ->take(5)
                ->whereNotin('a.id',$noit)
                ->orderBy('a.item_no','asc')
                ->get();
                if(!$data){
                  $data = DB::table('product as a')
                      //->leftjoin('selling_price_detail as b','a.id','=','b.product_id')
                      //->leftjoin('selling_price as c','c.id','=','b.selling_price_id')
                      ->leftjoin('unit as d','a.unit_id','=','d.id')
                      ->leftjoin('vwunitcon as e',function($q){
                        $q->on('a.unit_id','=','e.unit_id')->on('a.id','e.product_id');
                      })
                      ->leftjoin('vw_cogs_history as cogs',function($q){
                        $q->on('cogs.product_id','=','a.id');
                        //->on('cogs.branch_id','=','c.branch_id');
                      })
                      ->whereRaw("(a.code like '%".$request->get('code')."%' or a.name like '%".$request->get('code')."%')")
                      //->orWhere('a.name','like', '%' .$request->get('code').'%')
                      //->where('cogs.branch_id',$request->get('branch_id'))
                      //->where('c.customer_group_id',$request->get('customer_group_id'))
                      ->whereNull('a.deleted_at')
                      //->whereRaw('c.date = (select max(date) from selling_price)')
                      //->where('stk.warehouse_id',$request->get('warehouse_id'))
                      ->select('a.*',DB::raw('ifnull(cogs.price_balance,0) as selling_price'),'d.code as unit_name','e.qty as qtykali')
                      ->take(20)
                      ->whereNotin('a.id',$noit)
                      ->orderBy('a.item_no','asc')
                      ->get();
                }
          }else{
            $data = DB::table('product as a')
                ->leftjoin('selling_price_detail as b','a.id','=','b.product_id')
                ->leftjoin('selling_price as c','c.id','=','b.selling_price_id')
                ->leftjoin('unit as d','a.unit_id','=','d.id')
                ->leftjoin('vwunitcon as e',function($q){
                  $q->on('a.unit_id','=','e.unit_id')->on('a.id','e.product_id');
                })
                ->leftjoin('vw_cogs_history as cogs',function($q){
                  $q->on('cogs.product_id','=','a.id')
                  ->on('cogs.branch_id','=','c.branch_id');
                })
                //->where('a.code','like', '%' .$request->get('code').'%')
                //->orWhere('a.name','like', '%' .$request->get('code').'%')
                ->where('c.branch_id',$request->get('branch_id'))
                ->where('c.customer_group_id',$request->get('customer_group_id'))
                ->whereNull('a.deleted_at')
                ->whereRaw("(a.code like '%".$request->get('code')."%' or a.name like '%".$request->get('code')."%')")
                //->whereRaw('c.date = (select max(date) from selling_price)')
                //->where('stk.warehouse_id',$request->get('warehouse_id'))
                ->select('a.id','a.unit_id','a.name','a.item_no','a.code','b.price as selling_price','b.reg_disc_1','cogs.price_balance as cogs_balance','b.reg_disc_2','d.code as unit_name','e.qty as qtykali')
                ->take(5)
                ->whereNotin('a.id',$noit)
                ->orderBy('a.item_no','asc')
                ->get();
            // $data=DB::select("select a.name,a.item_no,a.code,b.price from product as a
            // left join (
            //     select product_id,price from selling_price_detail a left join 
            //     selling_price b on a.selling_price_id = b.id
            //     where customer_group_id =$request->get('customer_group_id')
            // )b on a.id = b.product_id
            //  where (a.code like '%".$request->get('code')."%' or a.name like '%".$request->get('code')."%')
            //  limit 5");
          }

        }
            // dd($data);
        return Response()->json([
            'data'=>$data
        ]);
    }
    public function getAllDataVue(Request $request){
      $tax="and a.tax=1";
      if($request->get('tax')!=""){
        if($request->get('tax')=='true'){
          $tax = "and a.tax=1";
        }else{
          $tax ="and a.tax=0";
        }
      }else{
        $tax="";  
      }  
      if($request->get('customer_group_id')!=""){
        $groupid=$request->get('customer_group_id');
      }else{
        $groupid =0;
      }
      $data=DB::select("select a.id as product_id,a.name,a.item_no,a.code,price,a.unit_id,c.code as unit_name,reg_disc_1,reg_disc_2,d.code as satuandasar,price_balance as cogs_balance,qtykali from product as a
            left join (
                select product_id,price,reg_disc_1,reg_disc_2 from selling_price_detail a left join 
                selling_price b on a.selling_price_id = b.id
                where customer_group_id=".$groupid."
            )b on a.id = b.product_id
            left join unit c on c.id = a.unit_id
            left join satuan_dasar d on a.id = d.id
            left join (select * from vw_cogs_history where branch_id=1)cogs on cogs.product_id = a.id 
             where 1=1
             $tax
             and
             (a.code like '%".$request->get('code')."%' or a.name like '%".$request->get('code')."%')
             limit 5");
         return Response()->json([
            'data'=>$data
        ]);
    }
    public function getAllSTproduct(Request $request){
      $noit = $request->get('no_id');
      $data = DB::select("select a.id,a.item_no,a.code,a.name as pname,ifnull(st.price,sp.spprice) as price from product a
left join (select product_id,price as spprice From selling_price_detail a left join selling_price b on a.selling_price_id = b.id
where b.customer_group_id='".$request->get('customer_group_id')."') sp on a.id = sp.product_id
left join (
select product_id,price from (
  select product_id,cast(selling_price*1.1 as decimal(10,2)) price from  vw_last_price_st
  where customer_id = ".$request->get('customer_id')."
  and product_id = ".$request->get('id')."
) a) st on a.id = st.product_id
where a.id='".$request->get('id')."'
");
      return Response()->json([
          'data'=>$data
      ]);
    }
    public function getAllsData(Request $request){
      $data = DB::table('product as a')->select('a.*','b.price as selling_price','b.reg_disc_1','b.reg_disc_2','d.code as unit_name','e.qty as qtykali')
            ->leftjoin('selling_price_detail as b','a.id','=','b.product_id')
            ->join('selling_price as c','c.id','=','b.selling_price_id')
          ->leftjoin('unit as d','a.unit_id','=','d.id')
          ->leftjoin('vwunitcon as e',function($q){
            $q->on('a.unit_id','=','e.unit_id')->on('a.id','e.product_id');
          })
          ->leftjoin('stockavl as f','a.id','=','f.product_id')
          //->where("stockavl",'>',0)
          ->where('a.code','like',"%".$request->get('code')."%")
          ->where('c.branch_id','=',$request->get('branch_id'))
          ->distinct()
          ->take(20)
          ->get();
        return Response()->json([
            'data'=>$data
        ]);
    }
    public function getAllDataNew(Request $request){
      if($request->get('notin')!=''){
        $notin = $request->get('notin');
      }else{
        $notin = "''";
      }
      $data = DB::select("select a.id,a.item_no,a.code,a.name,stockavl from product a
      left join (
        select * from stockavl where branch_id='".$request->input('branch_id')."' and warehouse_id ='".$request->input('wh')."'
        ) b on a.id = b.product_id not in (".$notin.")
        where (a.code like '%".$request->input('code')."%' or a.item_no like '%".$request->input('code')."%')
        limit 20");
        return Response()->json([
            'data'=>$data
        ]);
    }
   
    public function getDataById(Request $reg , $id){
       $data= DB::select("
       select b.code as name,a.product_id,cast(stockavl/qty as decimal(10,2)) total,whname,warehouse_id from vwunitcon as a
       left join unit as b on a.unit_id = b.id
       left join stockavl c on a.product_id = c.product_id
       where b.name is not null
       and a.product_id='".$id."'
       and warehouse_id = '".$reg->get('wh_id')."'
       order by a.unit_id asc");
        return Response()->json(
            ['msg'=>$data]
        );
    }
    
    
     public function updateDataFav(Request $request,$id)
    {
        $status = $request['favorite'];
        $users = Auth::user()->id;
        $update = DB::table('product')
        ->where('id',$id)
        ->update([
            'status' => $status,
            'updated_by' => $users
        ]);
        $product = DB::table('product')
        ->where('id',$id)
        ->select('code')
        ->first();
            return Response()->json(
                ['status' => true, 'msg' => $product->code.' Product added to favorite\'s','type'=>'success']
            );
    }
    public function getViewFav($id){
        $data = DB::table('product')->where('id',$id)
        ->select('status')
        ->first();
        return Response()->json([
          'msg'=>$data
        ]);
    }
    public function getViewFavNew($id){
        $data = DB::table('product')->where('id',$id)
        ->select('status')
        ->first();
        return Response()->json([
          'msg'=>$data
        ]); 
    }
    public function getUnitCon(Request $request){
        $data = DB::table('vwunitcon')->where('product_id',$request->get('prod_id'))->where('unit_id',$request->get('unit_id'))->select('qty')->get();
        return Response()->json(['msg'=>$data]);
    }
    public function getUnit(Request $request){
        $data = DB::table('vwunitcon as a')
        ->leftjoin('unit as b','a.unit_id','=','b.id')
        ->where('product_id',$request->get('prod_id'))
        ->whereNotNull('name')

        ->select('b.code as name','b.code','qty','a.unit_id')->get();
        return Response()->json(['msg'=>$data]);
    }

    public function getImageProduct($id){
        $data = DB::table('product_image')->where('product_id',$id)
        ->select('id','file_name')
        ->whereNotNull("file_name")
        ->get();
        $data2 = array();
        $i=0;
        foreach ($data as $key => $value) {
            $data2[$i]['id']=$value->id;
            $data2[$i]['file_name']=base64_encode($value->file_name);
            $i++;
        }
        return Response()->json([
          'msg'=>$data2
        ]);
    }
    public function getImageProductNew($id){
        $data = DB::table('product_image')->where('product_id',$id)
        ->select('id')
        ->get();
        return Response()->json([
          'msg'=>$data
        ]);   
    }
    public function getImageById($id){
      $data = DB::table('product_image')->where('id',$id)->first();
      return base64_encode($data->file_name);
    }
    public function getImageByIdNew($id){
      $data = DB::table('product_image')->where('id',$id)->first();
      return base64_encode($data->file_name); 
    }
   
    public function diUploads($id)
    {
      $data = DB::table('product_image')->where('product_id',$id)->get();
      $product = DB::table('product')->where('id',$id)->first();
      return view('modules.product.upload2',compact('id','data','product'));
    }
    public function uploadNew($id)
    {
      $data = DB::table('product_image')->where('product_id',$id)->get();
      $product = DB::table('product')->where('id',$id)->first();
      return view('modules.product.uploadNew',compact('id','data','product'));
    }
    public function uploadData2(Request $request, $id)
    {
      $data = DB::table('product')->where('id', $id)->first();
      $code = $data->code;
      $id = $data->id;
      $updated_at = $data->updated_at;
      $name_img = $code."-".$updated_at."-".$id.".".$request->file('files')->getClientOriginalExtension();
      // $folder = public_path('/usr/local/apache/htdocs/image');
      DB::table('product_image')->insert([
        'product_id'=>$id,
        'filename'=>$name_img,
        'updated_by'=>$request->session()->get('user_id')['id']
      ]);
      echo Storage::disk('sftp')->put($name_img, fopen($request->file('files'), 'r+'));

      return redirect('produk/diUpload/'.$id);
    }
    public function deleteUpload2($id)
    {
      $getid = DB::table('product_image')->where('id', $id)->first();
      DB::table('product_image')->where('id', $id)->delete();

      $path = '/usr/local/apache/htdocs/image'.'/'.$getid->filename;
      $file = Storage::files($path);
      Storage::delete($file);

      return redirect('produk/diUpload/'.$id);
    }
    public function deleteUploadNew($id)
    {
      $getid = DB::table('product_image')->where('id',$id)->first();
      DB::table('product_image')->where('id',$id)->delete();

      $path = '/usr/local/apache/htdocs/image'.'/'.$getid->filename;
      $file = Storage::files($path);
      Storage::delete($file);

      return redirect('productNew/upload/'.$id);
    }
    public function getImageProduk2($id)
    {
      $data = DB::table('product_image')->where('product_id', $id)
      ->select('id')
      ->get();
      return Response()->json([
        'msg'=>$data
      ]);
    }
    public function getImageById2($id)
    {
      $data = DB::table('product_image')->where('id', $id)->first();
      return base64_encode($data->filename);
    }                                                             

    public function ceksoByproduct(Request $reg,$id){
        $total = $reg->get('qty')*$reg->get('kali');
        $data = DB::select("select a.product_id,stockavl from sales_order_detail a
left join stockavl b on a.product_id = b.product_id
where a.deleted_at is null
and b.warehouse_id='".$reg->get('whid')."'
and b.product_id='".$reg->get('productID')."'
and b.stockavl>=".$total."
and a.sales_order_id=$id
order by a.product_id");
        if(count($data)){
          return Response()->json([
            'status'=>true,
            'title'=>'packing New',
            'msg'=>'',
            'type'=>'success'
          ]);
        }else{
          return Response()->json([
            'status'=>false,
            'title'=>'packing New',
            'msg'=>'opps!!',
            'type'=>'warning'
          ]);
        }
    }
    public function cekproduct($id, Request $reg){
        $dataproduct = DB::table('product')
        ->where('code',$id)
        ->orWhere('item_no','like','%'.$id)
        ->where('tax',$reg->get('tax'))
        ->first();
        if($dataproduct){
           $ceksalesorder =DB::table('sales_order_detail')
           ->where('sales_order_id',$reg->get('soid'))
           ->where('product_id',$dataproduct->id)
           ->get();
           if(count($ceksalesorder)>0){
              $cekpending=DB::table('vw_so_pending')
              ->where('sales_order_id',$reg->get('soid'))
              ->where('status','<>','Done')
              ->first();
              if($cekpending){
                  $cekStock =DB::table('stockavl')
                  ->where('branch_id',$reg->get('branch_id'))
                  ->where('warehouse_id',$reg->get('whid'))
                  ->where('product_id',$dataproduct->id)
                  ->first();
                  $cekjumlah=DB::table('temp_bound_serial_box')
                  ->select(DB::raw('sum(qty) as qty'))
                  ->where('sales_order_id',$reg->get('soid'))
                  ->where('product_id',$dataproduct->id)
                  ->groupBy('sales_order_id','product_id')
                  ->first();
                  if($cekStock){
                      if($cekStock->stockavl>0){
                            if($cekjumlah){
                              if(($cekjumlah->qty+1)<=$cekStock->stockavl){
                            DB::table('temp_bound_serial_box')
                            ->where('branch_id',$reg->get('branch_id'))
                            ->where('warehouse_id',$reg->get('whid'))
                            ->where('product_id',$dataproduct->id)
                            ->where('sales_order_id',$reg->get('soid'))
                            ->update([
                                'qty'=>$cekjumlah->qty+1,
                                'unit_qty'=>$cekjumlah->qty+1
                            ]);
                             }else{
                              return Response()->json([
                                'status'=>0,
                                'title'=>'stock tidak cukup',
                                'msg'=>'opps!!',
                                'type'=>'warning'
                              ]);
                            }
                          }else{
                            DB::table('temp_bound_serial_box')
                            ->insert([
                                'warehouse_id'=>$reg->get('whid'),
                                'branch_id'=>$reg->get('branch_id'),
                                'product_id'=>$dataproduct->id,
                                'sales_order_id'=>$reg->get('soid'),
                                'unit_qty'=>1,
                                'unit_id'=>1,
                                'unit_conversion'=>1,
                                'qty'=>1,
                                'updated_by'=>$reg->session()->get('user_id')['id']
                            ]);
                          }
                          return Response()->json([
                            'status'=>1
                          ]);
                       
                      }else{
                           return Response()->json([
                            'status'=>0,
                            'title'=>'Stock not avl',
                            'msg'=>'opps!!',
                            'type'=>'warning'
                          ]);
                      }
                  }else{
                      return Response()->json([
                        'status'=>0,
                        'title'=>'Stock not avl',
                        'msg'=>'opps!!',
                        'type'=>'warning'
                      ]);
                  }
              }else{
                return Response()->json([
                'status'=>0,
                'title'=>'Product not found in Sales Order',
                'msg'=>'opps!!',
                'type'=>'warning'
              ]);
              }
           }else{
              return Response()->json([
              'status'=>0,
              'title'=>'Product not found in Sales Order',
              'msg'=>'opps!!',
              'type'=>'warning'
            ]);
           }
        }else{
            return Response()->json([
            'status'=>0,
            'title'=>'Product not found',
            'msg'=>'opps!!',
            'type'=>'warning'
          ]);
        }
    }
   
}